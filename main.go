package main

import (
	"fmt"
	"os"
	"time"
	_ "time/tzdata"

	"github.com/gofiber/fiber/v2"
	uuid "github.com/satori/go.uuid"

	db "fiber-hexagonal/database"
	entityModel "fiber-hexagonal/integrations/entity_models"
	productRepo "fiber-hexagonal/integrations/repository/product"

	// unitRepo "fiber-hexagonal/integrations/repository/unit"

	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
)

func main() {
	initTimeZone()
	db.Connect()
	db.Db.AutoMigrate(
		&entityModel.Unit{}, &entityModel.Product{},
	)

	_productRepo := productRepo.NewProductRepository(db.Db)
	// _unitRepo := unitRepo.NewUnitRepository(db.Db)

	// setUnit := entityModel.Unit{
	// 	Code: "1",
	// 	Name: "test_unit",
	// }

	// createUnit, _ := _unitRepo.Create(setUnit)
	// fmt.Println("unit ", createUnit)

	getProduct, _ := _productRepo.GetById(uuid.FromStringOrNil("cb90a672-48b3-4655-8a71-0f7d7c7241be"))
	fmt.Println("product ", getProduct)
	getProduct.Name = "test update"

	createProduct, _ := _productRepo.Update(*getProduct)
	_ = createProduct
	fmt.Println("product ", createProduct)
	app := fiber.New(fiber.Config{
		BodyLimit: 25 * 1024 * 1024,
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			// Status code defaults to 500
			code := ctx.Context().Response.StatusCode()

			// Retrieve the custom status code if it's an fiber.*Error
			if e, ok := err.(*fiber.Error); ok {
				code = e.Code
			}

			// Send custom error page
			if err != nil {
				// In case the SendFile fails
				return ctx.Status(code).JSON(fiber.Map{
					"status":  fiber.StatusInternalServerError,
					"message": "internal_server_error",
				})
			}

			// Return from handler
			return nil

		},
	})
	app.Use(cors.New())
	app.Use(recover.New(recover.ConfigDefault))

	app.Use(logger.New(logger.Config{
		Format:     "${blue}${time} ${yellow}${status} - ${red}${latency} ${cyan}${method} ${path} ${green} ${ip} ${ua} ${reset}\n",
		TimeFormat: "02-Jan-2006 15:04:05",
		TimeZone:   "Asia/Bangkok",
		Output:     os.Stdout,
	}))

	if err := app.Listen(fmt.Sprintf(":%v", "8000")); err != nil {

	}

}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = ict
}
