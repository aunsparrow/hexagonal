package entitymodels

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

// Model struct
type Model struct {
	ID        uuid.UUID  `json:"id" gorm:"type:uuid;primaryKey"`
	Seq       int64      `json:"seq" gorm:"not null;autoIncrement:false;index"`
	CreatedAt time.Time  `json:"created_at" gorm:"index"`
	UpdatedAt time.Time  `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at" gorm:"index"`
}

// BeforeCreate hook table
func (m *Model) BeforeCreate(tx *gorm.DB) error {
	if uuid.Equal(m.ID, uuid.Nil) {
		m.ID = uuid.NewV4()
	}
	m.Seq = time.Now().UnixNano()
	return nil
}
