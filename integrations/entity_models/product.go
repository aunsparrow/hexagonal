package entitymodels

import (
	uuid "github.com/satori/go.uuid"
)

type Product struct {
	Model

	Code   string    `json:"code"`
	Name   string    `json:"name"`
	Price  float64   `json:"price"`
	UnitId uuid.UUID `json:"unit_id" `
	Unit   *Unit     `json:"unit" gorm:"references:id"`
}
