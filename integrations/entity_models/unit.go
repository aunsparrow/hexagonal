package entitymodels

type Unit struct {
	Model

	Code     string    `json:"code"`
	Name     string    `json:"name"`
	Products []Product `json:"product" gorm:"foreignKey:unit_id"`
}
