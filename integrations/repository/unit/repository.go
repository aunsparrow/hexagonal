package unit

import (
	entityModel "fiber-hexagonal/integrations/entity_models"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type UnitRepository struct {
	db *gorm.DB
}

func NewUnitRepository(db *gorm.DB) UnitRepository {
	return UnitRepository{db: db}
}

type IUnitRepository interface {
	GetAll() ([]entityModel.Unit, error)
	GetById(id uuid.UUID) (*entityModel.Unit, error)
	Create(model entityModel.Unit) (*entityModel.Unit, error)
}
