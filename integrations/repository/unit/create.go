package unit

import (
	entityModel "fiber-hexagonal/integrations/entity_models"

	"gorm.io/gorm"
)

func (r *UnitRepository) Create(model entityModel.Unit) (*entityModel.Unit, error) {

	if err := r.db.Model(&entityModel.Unit{}).Create(&model).Error; err != nil {
		if err == gorm.ErrRecordNotFound {

		}
		return nil, err
	}

	return &model, nil
}
