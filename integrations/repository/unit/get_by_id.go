package unit

import (
	entityModel "fiber-hexagonal/integrations/entity_models"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

func (r UnitRepository) GetById(id uuid.UUID) (*entityModel.Unit, error) {
	result := entityModel.Unit{}

	exc := r.db.Model(&entityModel.Unit{}).Where("id = ?", id)

	if err := exc.First(&result).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, err // code not found
		}
		return nil, err
	}

	return &result, nil
}
