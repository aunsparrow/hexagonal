package unit

import (
	entityModel "fiber-hexagonal/integrations/entity_models"

	"gorm.io/gorm"
)

func (r *UnitRepository) GetAll() ([]entityModel.Unit, error) {
	result := make([]entityModel.Unit, 0)
	err := r.db.Model(&entityModel.Unit{}).Find(&result)
	if err != nil {
		return result, err.Error
	}

	if err.Error == gorm.ErrRecordNotFound {

	}

	return result, nil
}
