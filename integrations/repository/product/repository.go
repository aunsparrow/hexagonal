package product

import (
	entityModel "fiber-hexagonal/integrations/entity_models"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type ProductRepository struct {
	db *gorm.DB
}

func NewProductRepository(db *gorm.DB) ProductRepository {
	return ProductRepository{db: db}
}

type IProductRepository interface {
	GetAll() ([]entityModel.Product, error)
	GetById(id uuid.UUID) (*entityModel.Product, error)
	Create(model entityModel.Product) (*entityModel.Product, error)
	Update(model entityModel.Product) (*entityModel.Product, error)
}
