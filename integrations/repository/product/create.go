package product

import (
	entityModel "fiber-hexagonal/integrations/entity_models"

	"gorm.io/gorm"
)

func (r ProductRepository) Create(model entityModel.Product) (*entityModel.Product, error) {

	if err := r.db.Model(&entityModel.Product{}).Create(&model).Error; err != nil {
		if err == gorm.ErrRecordNotFound {

		}
		return nil, err
	}

	return &model, nil

}
