package product

import (
	entityModel "fiber-hexagonal/integrations/entity_models"

	"gorm.io/gorm"
)

func (r ProductRepository) Update(model entityModel.Product) (*entityModel.Product, error) {

	if err := r.db.Model(&entityModel.Product{}).Where("id = ?", model.ID).Updates(&model).Error; err != nil {
		if err == gorm.ErrRecordNotFound {

		}
		return nil, err
	}

	return &model, nil

}
