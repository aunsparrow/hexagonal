package product

import (
	entityModel "fiber-hexagonal/integrations/entity_models"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

func (r ProductRepository) GetById(id uuid.UUID) (*entityModel.Product, error) {
	result := entityModel.Product{}

	exc := r.db.Model(&entityModel.Product{}).Where("id = ?", id)
	if err := exc.First(&result).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, err // code not found
		}
		return nil, err
	}

	return &result, nil
}
