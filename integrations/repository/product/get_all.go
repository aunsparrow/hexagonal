package product

import (
	entityModel "fiber-hexagonal/integrations/entity_models"

	"gorm.io/gorm"
)

func (r ProductRepository) GetAll() ([]entityModel.Product, error) {
	result := make([]entityModel.Product, 0)
	err := r.db.Model(&entityModel.Product{}).Find(&result)
	if err != nil {
		return result, err.Error
	}

	if err.Error == gorm.ErrRecordNotFound {

	}

	return result, nil
}
