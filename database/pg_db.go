package db

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Db *gorm.DB

func Connect() {
	username := "postgres"
	password := "Password#01"
	dbName := "golang_hexagonal"
	dbHost := "localhost"
	dbPort := "5432"

	dbUri := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s TimeZone=Asia/Bangkok", dbHost, dbPort, username, dbName, password) //Build connection string

	conn, err := gorm.Open(postgres.New(postgres.Config{
		DSN:                  dbUri,
		PreferSimpleProtocol: true, // disables implicit prepared statement usage
	}), &gorm.Config{})
	if err != nil {
		fmt.Print(err)
	}

	Db = conn
	fmt.Println("Database connection successfully")
}

func OpenDB() *gorm.DB {
	return Db
}
